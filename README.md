# Introduction

This repository was created by Gotam Dahiya for learning Deep Learning while applying it using TensorFlow library developed by Google.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequistes

Install these software for a smooth run
```
Python 3.5 or greater
Tensorflow 2.0 or greater
```

### Installing

Git clone the repository onto the local machine
``` https://gitlab.com/GotamDahiya/deep-learning.git```

## Contributing

Please fork the repository for making any contribution to this project. For updating any file please make a pull request and then update.

For references: [How to fork the repository](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork).

## Authors

* **Gotam Dahiya** - *[Gotam Dahiya](https://github.com/GotamDahiya)*
