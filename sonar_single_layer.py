import matplotlib.pyplot as plt
import tensorflow.compat.v1 as tf
import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder as LE
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split as TTS
# import tensorflow_probability as tfp

tf.disable_v2_behavior()


def read_dataset():
    df = pd.read_csv("sonar.csv")
    print(len(df.columns))
    X = df[df.columns[0:60]].values
    y=df[df.columns[60]]
    encoder = LE()
    encoder.fit(y)
    y = encoder.transform(y)
    Y = encode(y)
    return(X,Y,y)

def normalize_features(features):
    mu = np.mean(features,axis=0)
    sigma = np.std(features,axis=0)
    normalize_features = (features-mu)/sigma
    return normalize_features

def encode(labels):
    n_labels = len(labels)
    n_unique_labels = len(np.unique(labels))
    encoded_data = np.zeros([n_labels,n_unique_labels])
    encoded_data[np.arange(n_labels),labels] = 1
    return encoded_data

X,Y,y = read_dataset()
normalized_features = normalize_features(X)
X.shape

X,Y = shuffle(X,Y,random_state = 1)
train_x,test_x,train_y,test_y = TTS(X,Y,test_size=0.20,random_state=42)

print(train_x.shape)
print(train_y.shape)
print(test_x.shape)
print(test_y.shape)

learning_rate = 0.01
epochs = 2000

cost_history = np.empty(shape=[1],dtype=float)

n_dim = X.shape[1]
n_class = 2

x = tf.placeholder(tf.float32,[None,n_dim])
W = tf.Variable(tf.zeros([n_dim,n_class]))
b = tf.Variable(tf.zeros([n_class]))

init = tf.global_variables_initializer()

y_ = tf.placeholder(tf.float32,[None,n_class])
y = tf.nn.softmax(tf.matmul(x , W) + b)


cost_function = tf.reduce_mean(-tf.reduce_sum((y_ * tf.log(y)),1))
training_step = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost_function)

sess = tf.Session()
sess.run(init)
mse_history = []

for epoch in range(epochs):
    sess.run(training_step,feed_dict={x:train_x, y_:train_y})
    cost = sess.run(cost_function,feed_dict={x:train_x, y_:train_y})
    cost_history = np.append(cost_history,cost)
    pred_y = sess.run(y,feed_dict={x:test_x})
    print("epoch : ",epoch," - ","cost : ",cost)
    

correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction,tf.float32))
print("Accuracy: ",sess.run(accuracy,feed_dict={x:test_x,y_:test_y}))

plt.plot(range(len(cost_history)), cost_history)
plt.axis([0,epochs,0,np.max(cost_history)])
plt.show()
    